var sending = false;
var token = null;

function createRequest(type, url, data, callbacks, headers) {
	callbacks = callbacks || {};
	headers = headers || {};
	data = data === null || data === undefined
		? '' : JSON.stringify(data);

	var httpRequest = new XMLHttpRequest();
	httpRequest.open(type, url, true);


	for (var header in headers) {
		httpRequest.setRequestHeader(
			header, headers[header]
		);
	}

	for (var event in callbacks) {
		httpRequest.addEventListener(event, callbacks[event]);
	}

	httpRequest.send(data);
}

document.getElementById('get-resources')
.addEventListener('click', function (e) {
	e.preventDefault();

	if (token === null) {
		alert('Need Login.');
		return;
	}

	createRequest('POST', 'resource.php', null, {
		load: function () {
			var response = JSON.parse(this.responseText);
			alert(response.data);
		}
	}, {
		'Authorization': 'Token ' + token
	});
})

document.forms['register'].addEventListener('submit', function (e) {
	e.preventDefault();

	if (sending) {
		return;
	}

	var form = this;

	var data = {
		username: form.elements['username'].value,
		passowrd: form.elements['password'].value
	};

	sending = true;

	createRequest('POST', form.action, data, {
		loadend: function (e) {
			sending = false;
		},
		load: function (e) {
			var response = JSON.parse(this.responseText);
			token = response.data;
			console.log('Got token: ' + token);
		}
	}, {
		'Content-Type': 'application/json; charset=utf-8'
	});
});