<?php
	session_start();

	require_once(__DIR__ . '/inc/response.php');
	require_once(__DIR__ . '/inc/user.php');

	$SUPER_SECRET_DATA = array(
		'I Want to Believe',
		'UFOs... are they out there?',
		'We May Not Be Alone'
	);

	try {
		user_has_access();
		echo response($SUPER_SECRET_DATA, 200);
	}
	catch (\Exception $ex) {
		echo response($ex->getMessage(), 403);
	}