<?php
	function response($data = '', $status = 200) {
		return json_encode(array(
			'status' => $status,
			'data' => $data,
		));
	}