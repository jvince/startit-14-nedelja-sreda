<?php
	const HEADER_NOT_FOUND = 2000;

	function get_header($header) {
		$header = strtoupper('http_' . $header);

		if (!in_array($header, array_keys($_SERVER))) {
			throw new \Exception('Header not found.', HEADER_NOT_FOUND);
		}

		return $_SERVER[$header];
	}