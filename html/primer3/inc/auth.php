<?php
	require_once(__DIR__ . '/http.php');

	const AUTH_MISSING_TOKEN = 2000;

	function token_exists($token) {
		return $token === $_SESSION['token'];
	}

	function generate_token() {
		$token = bin2hex(openssl_random_pseudo_bytes(32));

		// TODO: Simuliramo snimanje tokena u bazu.
		$_SESSION['token'] = $token;

		return $token;
	}

	function token_expired($uid, $token) {
		// DB chech token and invalidate if needed.
		return FALSE;
	}

	function get_authorization_token() {
		$authorization_header = get_header('authorization');
		
		preg_match('/^Token\s(\S+)/',$authorization_header, $matches);

		if (empty($matches)) {
			throw new \Exception('Missing Token.', AUTH_MISSING_TOKEN);
		}

		return $matches[1];
	}