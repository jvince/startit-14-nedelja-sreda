<?php
	require_once(__DIR__ . '/auth.php');

	const USER_NOT_AUTHENTICATED = 1000;
	const USER_NOT_AUHTORIZED = 1001;

	function get_user($username, $password) {
		// throw new \Exception('User Error.', USER_NOT_AUTHENTICATED);
		
		return array(
			'uid' => 0,
			'username' => $username,
		);
	}

	function user_token_exists($uid) {
		return isset($_SESSION['token']);
	}

	function user_get_token($uid) {
		return !user_token_exists($uid)
			? generate_token($uid)
			: $_SESSION['token'];
	}

	function user_has_access() {
		$token = get_authorization_token();
		
		if (!token_exists($token)) {
			throw new \Exception('Not authorized.', USER_NOT_AUHTORIZED);
		}
	}