<?php
	session_start();
	
	require_once(__DIR__ . '/inc/response.php');
	require_once(__DIR__ . '/inc/auth.php');
	require_once(__DIR__ . '/inc/user.php');

	function validate($data) {
		// throw new \Exception('Validation Error.', VALIDATION_ERROR);
	}

	header('Content-Type: application/json; charset=utf-8');

	$raw = file_get_contents('php://input');

	try {
		$data = json_decode($raw, TRUE);

		validate($data);

		$user = get_user($data['username'], $data['password']);
		$token = user_get_token($user['uid']);

		echo response($token, 200);
	}
	catch (\Exception $ex) {
		echo response($ex->getMessage() . ' Code: ' . $ex->getCode(), 400);
		return;
	}
