<?php
	const VALIDATION_ERROR = 1000;
	const DB_ERROR = 2000;


	function validate($data) {
		// throw new \Exception('Validation Error.', VALIDATION_ERROR);
	}

	function insert_db($data) {
		// throw new \Exception('DB Error.', DB_ERROR);
	}

	function response($data = '', $status = 200) {
		return json_encode(array(
			'status' => $status,
			'data' => $data,
		));
	}

	header('Content-Type: application/json; charset=utf-8');

	$raw = file_get_contents('php://input');
	$data = json_decode($raw, TRUE);

	try {
		validate($data);
		insert_db($data);
	}
	catch (\Exception $ex) {
		echo response($ex->getMessage() . ' Code: ' . $ex->getCode(), 400);
		return;
	}

	echo response('User ' . $data['username'] . ' successfully registered.');