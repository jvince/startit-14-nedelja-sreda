var sending = false;

/**
 * @typedef {Object} UserData
 * @property {string} username Username
 * @property {string} password Password
 * @property {string} gender Gender
 */

/** 
 * @typedef {Object} HttpResponse
 * @property {number} status Status
 * @property {string} data Data
 */

/**
 * Kreira XMLHttpRequest
 * 
 * @param {string} type Tip HTTP zahteva (POST, GET)
 * @param {string} url Locakacija
 * @param {object} data Podaci za slanje
 * @param {object} callbacks Povratni pozivi
 */
function createRequest(type, url, data, callbacks) {
	var httpRequest = new XMLHttpRequest();
	httpRequest.open(type, url, true);
	httpRequest.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
	httpRequest.send(JSON.stringify(data));

	for (var event in callbacks) {
		httpRequest.addEventListener(event, callbacks[event]);
	}
}

document.forms['register'].addEventListener('submit', function (e) {
	e.preventDefault();

	if (sending) {
		return;
	}

	var form = this;

	/** @type {UserData} */
	var data = {
		username: form.elements['username'].value,
		passowrd: form.elements['password'].value,
		gender: form.querySelector('[name="gender"]:checked').value
	};

	sending = true;

	createRequest('POST', form.action, data, {
		loadend: function (e) {
			sending = false;
		},
		load: function (e) {
			/** @type {HttpResponse} */
			var response = JSON.parse(this.responseText);
			alert(response.data);
		}
	});
});