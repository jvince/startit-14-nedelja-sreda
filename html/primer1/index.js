// Za sve mogućnosti koje pruža API pogledati: http://www.icndb.com/api/
var RANDOM_JOKE_ENDPOINT = 'http://api.icndb.com/jokes/random';

// Uzimamo sve moguće statuse ako bi izbegli korišćenje switch naredbe
// prilikom ispisa trenutnog statusa http zahteva.
var HTTP_REQUEST_STATUS = Object.keys(XMLHttpRequest);

// Status.
var statusMessage = '';

// Kreiramo XMLHttpRequest objekat.
var httpRequest = new XMLHttpRequest();

// Reference DOM elemenata.
var $sendRequest = document.getElementById('send-request');
var $joke = document.getElementById('joke');
var $status = document.getElementById('status');

/**
 * Pomoćna funkcija kojom generišemo status poruke i ažuriramo vrednost $status
 * elementa.
 * @param {string} status - Poruka
 */
function updateStatus(status) {
	var date = new Date();
	var timestamp =
		date.getHours() + ':' +
		date.getMinutes() + ':' +
		date.getSeconds();
	$status.value += timestamp + ' ' + status + '\n';
}

httpRequest.addEventListener('readystatechange', function (e) {
	updateStatus(HTTP_REQUEST_STATUS[httpRequest.readyState]);
});

httpRequest.addEventListener('load', function (e) {
	try {
		var response = JSON.parse(httpRequest.responseText);

		if (response.type === 'success') {
			var data = response.value;
			$joke.innerHTML = data.joke;
		}
	}
	catch (exception) {
		alert(exception.message);
	}
});

httpRequest.addEventListener('loadend', function (e) {
	document.body.classList.remove('loading');
});

httpRequest.addEventListener('error', function (e) {
	updateStatus('ERRORED');
	updateStatus(httpRequest.status + ' ' +  httpRequest.statusText);
});

httpRequest.addEventListener('abort', function (e) {
	updateStatus('ABORTED');
});

$sendRequest.addEventListener('click', function () {
	if (httpRequest.readyState === (httpRequest.OPENED || httpRequest.LOADING)) {
		httpRequest.abort();
	}

	httpRequest.open('GET', RANDOM_JOKE_ENDPOINT, true);
	httpRequest.send();
	document.body.classList.add('loading');
});